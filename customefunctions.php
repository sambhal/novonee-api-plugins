<?php
/**
 * Plugin Name: My Custom Functions
 * Plugin URI: http://novonee.com
 * Description: This is an awesome custom plugin with functionality that I'd like to keep when switching things.
 * Author: Suhail Akhtar
 * Author URI: http://sprvtec.com
 * Version: 0.1.0
 */

/******************************** CODE BY SUHAIL AKHTAR APRIL 2017 ****************/




/*=============================================
=            Section cancel_subscription_befor_user_deleted           =
=============================================*/


function cancel_subscription_befor_user_deleted( $user_id ) {

    gf_stripe()->log_debug( "Entered cancel_subscription_befor_user_deleted " );

    $entry_id = get_user_meta( $user_id, '_gform-entry-id', true );
    if(!empty($entry_id)) {

        $entry = GFAPI::get_entry( $entry_id );
        $feed = gf_stripe()->get_payment_feed( $entry );
        $result = gf_stripe()->cancel( $entry, $feed );
        gf_stripe()->log_debug( "gform_post_add_subscription_payment: Cancelling subscription (feed #{$feed['id']} - {$feed_name}) for entry #{$entry['id']}. Result: " . print_r( $result, 1 ) );
        // mail('suhail@sprvtec.com','Novonee user delete',$result);

    } else {

        gf_stripe()->log_debug( "User was not registered with feed. User id ". $user_id );
        gf_stripe()->log_debug( "Searching from stripe api using email" );


        $stripe_sk = "sk_live_mmqgpMnUzt0aktVOD0oBe8Ap";
        // $stripe_sk = "sk_test_AT5q5EyUlr6yVs13AkFgaQGY";


        $curr_user = get_user_by('id', $user_id);
        gf_stripe()->log_debug('User data'. print_r($curr_user) );
        $curr_user_email = $curr_user->data->user_email;     
        gf_stripe()->log_debug('User email'. print_r($curr_user_email) );

        $url = 'https://api.stripe.com/v1/search?query='. $curr_user_email .'&prefix=false';
        $args = array(
            'headers' => array(
                "content-type" => "application/x-www-form-urlencoded",
                'Authorization' => 'Bearer '.$stripe_sk  
                )
            );
        $response = wp_remote_get( $url, $args );

        $api_response = json_decode( wp_remote_retrieve_body( $response ),true );
        $body = json_decode($response['body']);
        gf_stripe()->log_debug('API data'. print_r($body) );

        $cid = $body->data[0]->id;
        $subscription_id = $body->data[0]->subscriptions->data[0]->id;
        if($subscription_id) {

            $url = 'https://api.stripe.com/v1/subscriptions/'.$subscription_id;
            $args = array(
                'method' => 'DELETE',
                'headers' => array(
                    "content-type" => "application/x-www-form-urlencoded",
                    'Authorization' => 'Bearer '.$stripe_sk  
                    )
                );
            $response = wp_remote_request( $url, $args );

            $api_response = json_decode( wp_remote_retrieve_body( $response ),true );
            $body = json_decode($response['body']);
            gf_stripe()->log_debug( "Subscription cancel response using stripe api. Result: " . print_r( $body->status, 1 ) );

        } else {
            gf_stripe()->log_debug( "User was not Found even in the stripe" );
            gf_stripe()->log_debug( "Subscription cancellation Failed." );
            echo "User subscription cancellation Failed";
            return ;

        }

    }

}


add_action( 'delete_user', 'cancel_subscription_befor_user_deleted' );

/*=====  End of cancel_subscription_befor_user_deleted  ======*/



/*==================================================================================
=            After login check user role and redirect if not subscribed            =
==================================================================================*/


add_action('wp', function(){

    ob_flush();
    ob_start();
    $user = wp_get_current_user();

    $url = get_site_url()."/members/".$user->user_login."/payment-method/";
    if ( in_array( 'payment-failed', (array) $user->roles ) ) {
        
        $current_url=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
     
        if(!strpos($url, $current_url)) {

            $message = "Your payment is failed.";
            echo "<script type='text/javascript'>//console.log('$message');</script>";
            echo "<script>
            alert('It looks like your credit card on file needs updating. Click on OK to be re-directed to the Payment Method page and update your credit card details. If you are still having trouble, please email the administrator directly at support@novonee.com.');
            window.location.href='".$url."';
            </script>";
            exit;
        }

    }

});


/**
 *
 * Stripe Webhooks
 *
 */

add_filter( 'gform_stripe_webhook', 'stripe_webhook_custom_action', 10, 2 );
function stripe_webhook_custom_action( $action, $event ) {
    gf_stripe()->log_debug(__METHOD__ . '(): Entered');
    gf_stripe()->log_debug(__METHOD__ . '(): Event '. print_r($event,1));
	global $wpdb;
	$type = $event->type;
	$transaction_id =  $action['transaction_id'] = rgars( $event, 'data/object/id' );
	$customer =  $action['transaction_id'] = rgars( $event, 'data/object/customer' );

	if($type=='charge.succeeded' || $type == 'charge.failed') {
		if ( function_exists( 'gf_user_registration' ) ) {
			$entry_id = gf_stripe()->get_entry_by_transaction_id( $action['transaction_id'] );
			if(!$entry_id) {
				$sql = $wpdb->get_results( "SELECT lead_id  FROM `".$wpdb->prefix."rg_lead_meta` WHERE meta_key='stripe_customer_id' and meta_value='".$customer."'" );
				$entry_id = $sql[0]->lead_id;
				gf_stripe()->log_debug( __METHOD__ . '(): SQL : '.print_r($sql),1 );	
			}
			gf_stripe()->log_debug( __METHOD__ . '(): entry_id : '.$entry_id );	
			$user = gf_user_registration()->get_user_by_entry_id( $entry_id );
            gf_stripe()->log_debug( __METHOD__ . '(): USER : '.print_r($user),1 );   

			if ( is_object( $user ) ) {
				gf_stripe()->log_debug( __METHOD__ . '(): Action type is '.$type );	
				gf_stripe()->log_debug( __METHOD__ . '():User is '.print_r($user,1) );	
				if($type=='charge.succeeded') {

					if ( in_array( 'payment-failed', (array) $user->roles ) ) {
						$user->remove_role( 'payment-failed' );

						$to = $user->data->user_email;
						$subject = 'Your Novonee payment is successful ';
						$body = 'Success!  Your membership in the Premier Dentrix Online Community is updated and back online.  You can now log back in and enjoy all the benefits Novonee has available.';
						send_mail($to,$subject,$body);

						$action['type']     = 'success_payment';
						gf_stripe()->log_debug( __METHOD__ . '(): User had payment-failed role and it is now removed after successful payment. ' );	
					}
				} else if($type=='charge.failed'){
					$user->add_role( 'payment-failed' ); 
					$to = $user->data->user_email;
					$subject = 'Your Novonee subscription payment is failed';
					$body = 'Ooops!  Your membership with Novonee – The Premier Dentrix Online Community needs attention.  Please login at <a href="www.novonee.com">www.novonee.com</a> to update your credit card details.  If you have forgot your password to login, click on Lost Password to reset.  If you have lost all your login credentials please reply to this email and the administrator will be notified.';
					send_mail($to,$subject,$body);
					
					gf_stripe()->log_debug( __METHOD__ . '(): Payment-failed user role is set to payment-failed.' );

				}

				$action['entry_id'] = $entry_id;
				$action['amount']   = gf_stripe()->get_amount_import( rgars( $event, 'data/object/amount' ), $entry['currency'] );
			} else {
				gf_stripe()->log_debug( __METHOD__ . '(): User not found' );	
			}
		}
		
	}

	
    gf_stripe()->log_debug(__METHOD__ . '(): Exit');


	return $action;
}

// Dynamically add state2 variable in Gravity form submitted $_POST
add_action( 'gform_pre_process_2', function( $form ) {
    $field_values = rgpost( 'gform_field_values' );
    $_POST['state_2'] = GFFormDisplay::get_state( $form, $field_values );
    // If the submission of form is through api then apply the coupon to remove product and payment inputs.
    if($_POST['is_submission_by_api']) {
        $_POST['input_10']  = 'free';
    }
} );


// Run this when join form is submitted and get IAP data. it will be called when the user is registered by clicking on confirmation email but we have to do this after the form is submitted.
function save_user_iap_data( $entry, $form) {

    // @wp_mail('sambhal@outlook.com',' POST', json_encode($_POST) );
 $entry_id = $entry['id'];
 $payment_receipt = rgar( $entry, '11' );

    // If the form is sent through api then input_11 contains the inapp purchase data then insert it into database otherwise return it.
 if($payment_receipt == "") return;

 global $wpdb;
 $query = "INSERT INTO `" . $wpdb->prefix . "iap_purchase` (`id`, `lead_id` , `user_id`, `payment_receipt`) VALUES (NULL, '$entry_id' , NULL, '$payment_receipt');";
 $wpdb->query( $query );

}
add_action( 'gform_after_submission_2', 'save_user_iap_data', 10, 4 );

/**
 *
 * When user activates his a/c this function will be called to update the user_id in the wp_iap_purchase table. 
 * If the user is registered through IOS app then table will be used to verify the IAP purchase.
 *
 */

function update_iap_purchase_user_id( $user_id, $feed, $entry, $user_pass ) {    

    $lead_id = $entry['id'];
    
    global $wpdb;
    $query = "UPDATE `" . $wpdb->prefix . "iap_purchase` SET user_id = $user_id WHERE lead_id = $lead_id;";
    $wpdb->query( $query );


}
add_action( 'gform_user_registered', 'update_iap_purchase_user_id', 10, 4 );


function send_mail($to,$subject,$body) {

	$headers = array('Content-Type: text/html; charset=UTF-8');
    $headers[] = 'From: Novonee <support@novonee.com> ' . "\r\n";
    $headers[] = 'Bcc: <dayna@novonee.com>';
    $headers[] = 'Reply-To: Novonee <support@novonee.com> ' . "\r\n";
    wp_mail( $to, $subject, $body, $headers );
}

/* When user logins check if the user is apple subscriber and check for subscription if his subscription is expired then logout and prevent from accessing website */
function check_ios_subscription_on_website() {
    
    $user_id = get_current_user_id();
    $url = get_site_url().'/wp-json/sprv/v1/CheckIosSubscription?user_id='.$user_id;
    $request = wp_remote_get($url);

    if (is_wp_error($request)) {
        // return false;
    }

    $body = wp_remote_retrieve_body($request);
    $data = json_decode($body);
    if ($data->Result) {
        if (!$data->is_subscription_active) {
            $message = "Your iOS subscription is expired. Please renew your subscription visiting iTunes.";
            wp_logout();
            echo "<script type='text/javascript'>console.log('".__LINE__ ." : ". $message."');</script>";
            echo "<script>alert('".$message."');window.location.href = '".get_site_url()."'; </script>";
            exit;
        }
    } else {
        $message = $data->Message;
        wp_logout();
        echo "<script type='text/javascript'>console.log('".__LINE__ ." : ". $message."');</script>";
        echo "<script>alert('".$message."');window.location.href = '".get_site_url()."'; </script>";
        exit;

    }

}
add_action('wp','check_ios_subscription_on_website');
