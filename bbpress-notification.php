<?php
/*
Plugin Name: BbPress Notification to Ionic App
Plugin URI:  http://sprvtec.com/
Description: Send new topic and reply notification to all subscribers on mobile apps.
Version: 1.0
Author: Suhail Akhtar
Author URI: http://sprvtec.com
License: GPLv3
*/
if (!defined('ABSPATH'))
{
	exit;
}

/**
 *
 * It sends the notification to the subscribed user's of a forum whenever a new topic is created in that forum
 *
 */

function new_topic_notification($topic_id , $topic_author , $anonymous_data , $topic_author2 )
{
    $admin_email = get_option('admin_email');
    $topic_title = html_entity_decode(strip_tags(bbp_get_topic_title($topic_id)), ENT_NOQUOTES, 'UTF-8');
    $topic_author = bbp_get_topic_author($topic_id);

    // Get all the forum subscribers
    $forum_id = bbp_get_topic_forum_id($topic_id);
    $subscribers = bbp_get_forum_subscribers($forum_id); 
    $_tokens = [];

    // Get the device tokens of all subscribed users
    $_tokens = get_all_subscribers_device_token($subscribers);
   
     // Prepare the notification fields
    
    $data               = [];
    $data['url']        = '#/app/topic/'.$topic_id ;
    $data['title']      = "New Topic by ".$topic_author ;
    $data['state']      = 'app.topic' ;
    $data['param']      = 'topicId' ;
    $data['value']      = $topic_id ;
    $data['tokens']     = $_tokens;
    $data['message']    = $topic_title;

    sprv_send_push_notification($data);
}

/**
 *
 * It sends the notification to the subscribed user's of a topic whenever a new reply is created in that topic
 *
 */
function new_reply_notification( $reply_id ) 
{
	global $wpdb;
    $admin_email = get_option('admin_email');
   	$reply_content = bbp_get_reply_content(  $reply_id ); 
    $topic_id = get_post_meta( $reply_id, '_bbp_topic_id', TRUE );
    $forum_id = get_post_meta( $reply_id, '_bbp_forum_id', true );
    $reply_author = bbp_get_reply_author( $reply_id );
    $topic_title = bbp_get_topic_title( $topic_id );
    
    // get all subscribers of topic
    $subscribers = bbp_get_topic_subscribers($topic_id); 
    $_tokens = get_all_subscribers_device_token($subscribers);
   		
    $data              = [];
    $data['url']       = '#/app/topic/'.$topic_id ;
    $data['title']     = $reply_author . " replied to " . $topic_title ;
    $data['state']     = 'app.topic' ;
    $data['param']     = 'topicId' ;
    $data['value']     = $topic_id ;
    $data['tokens']    = $_tokens;
    $data['message']   = $reply_content;

    sprv_send_push_notification($data);

}

/**
 * This function takes user_id as param and return the latest device token for that user
 * @param user_id
 * @return device_token
 */
function sprv_get_push_token_by_user_id($user_id){

	global $wpdb;
	$query  = 'SELECT * FROM wp_sprv_ionic_tokens WHERE user_id = '.$user_id . ' order by id DESC limit 1';
	$results = $wpdb->get_results( $query );
	return  $results[0]->device_token;
}
/**
 *
 * It returns the device token of all the subscribers of a topic or forum
 * @param subscriber's user_id array or single id
 * @return Array of device tokens
 *
 */

function get_all_subscribers_device_token($subscribers){
	
    if(gettype($subscribers) != 'array' ) {
        $_tokens[$subscribers] = sprv_get_push_token_by_user_id($subscribers);
        return $_tokens;
    }
	foreach ($subscribers as $key => $user_id) {
		$_tokens[$user_id] = sprv_get_push_token_by_user_id($user_id);
	}
	return $_tokens;
}

/**
 *
 * This is the actual function from where the notification is sent.
 * @param Array of notification data such as title, message, tokens, url etc
 *
 */
    
function sprv_send_push_notification($data){
    ini_set('max_execution_time', 300); // 300 seconds = 5 minutes
    create_notification_entry_in_db($data);

    // It is used to keep each notification on top of old notification. If its not used then new notification will replace the old notification.
    
    $temp_tokens_with_id = array_filter($data['tokens'], "remove_nulls");
    // Now this array contains user id as key and token as value
    $data['tokens'] = array_values($data['tokens']);
    // Remove the null tokens from the token array
    $data['tokens'] = array_values(array_filter($data['tokens'], "remove_nulls"));
   
    foreach ($temp_tokens_with_id as $user_id => $token) {

        static $notId;
        $d           = time();
        $notId       = $d; 
        $badge_count = get_unread_notifications(array('user_id' => $user_id));
        $badge_count = ($badge_count == null) ? 1 : $badge_count;

        $fields                                             = [];
        $fields['to']                                       = $token;
        $fields['data']                                     = [];
        $fields['data']['url']                              = $data['url'] ;
        $fields['data']['token']                            = $token;
        $fields['data']['state']                            = $data['state'];
        $fields['data']['param']                            = $data['param'] ;               
        $fields['data']['value']                            = $data['value'] ;
        $fields['data']['user_id']                          = $user_id;
        
        $fields['notification']                             = [];
        $fields['notification']['body']                     = $data['message'];
        $fields['notification']['title']                    = $data['title'];
        $fields['notification']['badge']                    = $badge_count;



        $fields['notification']['ios']                      = [];
        $fields['notification']['android']                  = [];
        $fields['notification']['ios']['data']              = [];
        $fields['notification']['android']['data']          = [];

        $fields['notification']['ios']['badge']             = $badge_count;
        $fields['notification']['ios']['data']['notId']     = $notId;
        $fields['notification']['android']['data']['notId'] = $notId;
        $fields['notification']['android']['data']['badge'] = $badge_count;

        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: key=AAAAy8Sfhik:APA91bGe2UFscAlH8JTv7ABBvrkWuZLVY6kYCseRTpZ2DrbKQhfUUcfYpX1ZUYbw9HxSHWZDLeyksAHzy1kZN66lT0WImeEMv-qeLLiyBltVxJwwXDLfTmQYqqmrtjzYUBT8mNOqieTq'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        $notId += 10;

        // email_to_admin(json_decode($response));
    }
}

/**
 *
 * This function sends notification to all the users whenever a new event is created
 * @param result and event object of newly created event
 *
 */

function em_event_send_notification($result, $EM_Event){

        $event_id = $EM_Event->post_id;
        $event_title = $EM_Event->post_title;
        em_event_send_notification_send($event_id, $event_title);

}

function em_event_send_notification_send($event_id, $event_title) {
    $users = get_users( array( 'fields' => array( 'ID' ) ) );
    $user_ids = [];
    foreach($users as $user){
        $user_ids[] = $user->ID;
    }
    $_tokens           = get_all_subscribers_device_token($user_ids);
    $data              = [];
    $data['url']       = '#/app/events/'.$event_id ;
    $data['title']     = 'New Event is coming' ;
    $data['state']     = 'app.event' ;
    $data['param']     = 'eventId' ;               
    $data['value']     = $event_id ;
    $data['tokens']    = $_tokens;
    $data['message']   = $event_title;
    sprv_send_push_notification($data);
}

/**
 *
 * This function removes the null tokens from token array
 *
 */

function remove_nulls($val){
    return !is_null($val);
}

/**
 *
 * It sends the notification whenever a new private msg is sent to any user
 *
 */

function new_message_notification($message){

    $thread_id      = $message->thread_id;
    $sender_id      = $message->sender_id;
    $sender         = get_userdata(  $sender_id );
    $recipients     = array_values((array) $message->recipients);
    $recipients_ids = [];
    foreach ($recipients as $key => $value) {
        $recipients_ids[] = $value->user_id;
    }

    $_tokens           = get_all_subscribers_device_token($recipients_ids);
    $data              = [];
    $data['tokens']    = $_tokens;
    $data['message']   = $message->message;
    $data['title']     = $sender->user_nicename. ' sent you a new message :"' .$message->subject. '"' ;
    $data['payload']   = [] ;
    $data['state']     = 'app.message' ;
    $data['param']     = 'id' ;               
    $data['value']     = $thread_id ;
    $data['url']       = '#/app/messages/view/'.$thread_id ;

    sprv_send_push_notification($data);

}

/**
 *
 * It sends the notification whenever a new public msg is sent to any user
 *
 */

function new_public_message_notification($r){
    
    $sender            = get_userdata($r['user_id']);
    // $to                = $r['item_id'];
    $message           = $r['content'];

    // Public message sent through app 
    $to = [];
    $mentions = bp_activity_find_mentions($message);
   
    foreach ($mentions as $key => $mention) {
        $to [] = bp_activity_get_userid_from_mentionname($mention);
    }
    
    $_tokens           = get_all_subscribers_device_token($to);
    $data              = [];
    $data['tokens']    = $_tokens;
    $data['message']   = $message;
    $data['title']     = $sender->user_nicename. ' sent you a public message' ;
    $data['state']     = 'app.activity' ;
    $data['url']       = '#/app/activity' ;
    sprv_send_push_notification($data); 
}

/**
 *
 * It sends the notification to all users for those events that are coming in next week 
 *
 */

function send_event_reminders(){

    require_once(WP_PLUGIN_DIR.'/events-manager/events-manager.php');

    $data              = [];
    $upcoming_events   = [];
    $events            = sprv_get_future_events();

    foreach ($events->data as $key => $event) {
        $data[$event['event_id']]                       = [];
        $data[$event['event_id']]['post_id']            = $event['post_id'];
        $data[$event['event_id']]['event_name']         = $event['event_name'];
        $data[$event['event_id']]['event_start_date']   = $event['event_start_date'];

        $d      = strtotime("+2 day");
        $date   = date('Y-m-d',$d);

        // Notification is for those events that are coming in next week
        if($event['event_start_date'] <= $date) {
            $upcoming_events[] =  $data[$event['event_id']];
        }
    } 
    foreach ($upcoming_events as $key => $event) {
        em_event_send_notification_send($event['post_id'], $event['event_name']);
    }
}

function email_to_admin($data){

    $admin_email = get_option('admin_email');
    $admin_email = 'sambhal@outlook.com';
    @wp_mail( $admin_email, __METHOD__, json_encode($data) );
}

/**
 *
 * This functions stores all the notifications in wp_sprvionic_notifications table
 *
 */

function create_notification_entry_in_db($data) {

    $user_ids = array_keys(array_filter($data['tokens'], "remove_nulls"));

    global $wpdb;
    $rows = [];
    $values = [];
    foreach($user_ids as $key => $user_id) {

        $rows[$key] = [];
        $rows[$key]['user_id']  = $user_id;
        $rows[$key]['title']    = $data['title'];
        $rows[$key]['message']  = $data['message'];
        $rows[$key]['url']      = $data['url'];
        $rows[$key]['state']    = $data['state'];
        $rows[$key]['param']    = $data['param'];
        $rows[$key]['value']    = $data['value'];
        $rows[$key]['status']   = 0;
        // Check if the notification for current day is already in the database then don't add in the db
        $notification_exists = is_notification_in_the_db_for_today($user_id, $data['url']);
        if(!$notification_exists) {
            $values[] = $wpdb->prepare("(%d,%s,%s,%s,%s,%s,%s,%d)", $user_id, $data['title'], $data['message'], $data['url'], $data['state'], $data['param'], $data['value'], 0);
        }
    }

    $query = "INSERT INTO ".$wpdb->prefix.
    "sprv_ionic_notifications (user_id, title, message, url, state, param, value, status) VALUES ";
    $query .= implode(",", $values);
    $exec = $wpdb->query($query);

}

function get_unread_notifications($data){

    $user_id = $data['user_id'];

    global $wpdb;
    $q = "SELECT count(*) total FROM " . $wpdb->prefix . "sprv_ionic_notifications WHERE status=0 and user_id=".$user_id;
    $unread = $wpdb->get_results($q)[0]->total;
    return $unread;

}

function get_platform_by_user_token($token){

    global $wpdb;
    $q = "SELECT * FROM " . $wpdb->prefix . "sprv_ionic_tokens WHERE device_token=".$token;
    $platform = $wpdb->get_results($q)[0]->platform;
    return $platform;

}


function sprv_bp_activity_new_comment_notification_helper( $comment_id = 0, $params = array() ) {

    $to = [];
    $commenter_id      = $params['user_id'];
    $poster_name       = bp_core_get_user_displayname( $commenter_id );
    $original_activity = new BP_Activity_Activity( $params['activity_id'] );

    // If comment is a comment for child comment then take the user_id of this comments parent comment
    if($params['activity_id'] != $params['parent_id']){
        $parent_activity = new BP_Activity_Activity( $params['parent_id'] );
        if($parent_activity->user_id != $commenter_id ) $to[] = $parent_activity->user_id;
    }

    // If the commenter is not the activity poster then send add to $to 
    if($original_activity->user_id != $commenter_id ) $to[] = $original_activity->user_id;
    if(empty($to)) return;
    
    $content = $params['content'];

    $_tokens           = get_all_subscribers_device_token($to);
    $data              = [];
    $data['tokens']    = $_tokens;
    $data['message']   = $content;
    $data['title']     = $poster_name . ' posted a comment on activity.' ;
    $data['state']     = 'app.activity' ;
    $data['url']       = '#/app/activity' ;
    sprv_send_push_notification($data); 
}


function is_notification_in_the_db_for_today($user_id, $url){

    $current_date = date('Y-m-d');
    global $wpdb;
    $q = "SELECT count(*) FROM " . $wpdb->prefix . "sprv_ionic_notifications WHERE user_id=".$user_id . " AND url = '" .$url."' AND date(date_created) = '" .$current_date. "';";
    $count = $wpdb->get_var($q);
    return $count;

}
/*==========================================================================================
=            This is the section for cron job for events reminder notifications            =
==========================================================================================*/

// Uncomment this block if you want to send notifications from wp itself. 
// Currently I am using cpanel for cron jobs.
/*
// Add one minute interval to cron recurrence  
function isa_add_cron_recurrence_interval( $schedules ) {
    $schedules['every_three_minutes'] = array(
            'interval'  => 60, // change it for changing the interval of cron  (seconds)
            'display'   => __( 'Every 3 Minutes', 'textdomain' )
    );
     
    return $schedules;
}
add_filter( 'cron_schedules', 'isa_add_cron_recurrence_interval' );

// Schedule the for cron job
if ( ! wp_next_scheduled( 'schedule_event_reminders' ) ) {
  wp_schedule_event( time(), 'every_three_minutes', 'schedule_event_reminders' );
}
// On this cron call send_event_reminders function to send notifications
add_action( 'schedule_event_reminders', 'send_event_reminders' );
*/

/*=====  End of This is the section for cron job for events reminder notifications  ======*/

add_action('bbp_new_topic', 'new_topic_notification');
add_filter('em_event_save','em_event_send_notification',10,2);
add_filter('messages_message_sent','new_message_notification');
add_filter('bp_activity_add','new_public_message_notification');
add_action('bbp_new_reply_post_extras', 'new_reply_notification');
add_action('bp_activity_comment_posted', 'sprv_bp_activity_new_comment_notification_helper', 10, 2 );


// Add new url to wp json api for sending daily events reminder to all users
add_action( 'rest_api_init', function(){

    $args1 = array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'send_event_reminders',
        );
    register_rest_route( 'sprv/v1', '/'.'SendEventReminders', $args1 );
} ); 

// API for getting unread notifications
add_action( 'rest_api_init', function(){

    $args1 = array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'get_unread_notifications',
        'args' => array(
            'user_id' => array(
                'type'               => 'integer'
            )
        )
        );
    register_rest_route( 'sprv/v1', '/'.'unread_notifications', $args1 );
} ); 

?>