<?php

// This the required library to verify the Inapp purchase receipt for IOS
require_once 'AppleInAppPurchaseVerification.php';
use AppleIAP\AppleInAppPurchaseVerification;
/*
 * Return images for application homepage
*/
function sprv_get_homepage_images($request){

	$return = [];

	$upload_dir = wp_upload_dir()['basedir'];
	$dir = $upload_dir . '/novonee_app_resources/*';
	$i = 0;
	foreach ( glob( $dir ) as $filename) {
		$return[$i] = [];
		$return[$i]['ImageUrl'] = str_replace($upload_dir,wp_upload_dir()['baseurl'] , $filename);
		$return[$i]['ImageType'] = 0;
		$i++;
	}

	return new WP_REST_Response($return, 200);

}

function sprv_save_device_token($data){
    
    global $wpdb;

    $result = [];
    
    $wpdb->replace( 
        'wp_sprv_ionic_tokens', 
        array( 
            'user_id' => $data['user_id'], 
            'platform' => $data['platform'], 
            'device_token' => $data['device_token'] 
        ), 
        array( 
            '%d',
            '%s', 
            '%s' 
        ) 
    );

    return new WP_REST_Response($result, 200);
    
}

function sprv_save_device_token_bkp($data){
	
	global $wpdb;

	$result = [];

	$user_id = $data['user_id'];

	$device_token = $data['device_token'];

	$result['result'] = $wpdb->insert( 
	'wp_sprv_ionic_tokens', 
	array( 
		'user_id' => $user_id, 
		'device_token' => $device_token 
	), 
	array( 
		'%d',
		'%s'
	) 
);

	return new WP_REST_Response($result, 200);
	
}

function sprv_get_notifications($request){
	
	// If user is not logged in then return error
	if(!is_user_logged_in()){
		return new WP_Error( 'Authentication Failed', sprintf( __( 'User is not logged in. Please login and try again.') ), array( 'status' => 404 ) );
	}

	$page 		= $request->get_params()['page'] ;
	$per_page 	= $request->get_params()['per_page'];
	if($per_page > 100 ) $per_page = 20;

	$offset = ($page-1)*$per_page; 



	global $wpdb;
	$query  = 'SELECT * FROM wp_sprv_ionic_notifications WHERE user_id = '.get_current_user_id() ;
	$query .= ' ORDER BY notification_id DESC  limit ';
	$query .= $per_page .' offset '.$offset;
	$results = $wpdb->get_results( $query );
	$result['query'] = $query;

	$query1  = $wpdb->get_results('SELECT * FROM wp_sprv_ionic_notifications WHERE user_id = '.get_current_user_id()) ;
	$total = $wpdb->num_rows;
	$total_pages = ceil($total/$per_page);

	header('X-WP-Total:'. $total);
	header('X-WP-TotalPages:'. $total_pages);

	return $results;
}

function sprv_notification_set_read($data){

   global $wpdb;

  $result = $wpdb->update( 
    $wpdb->prefix.'sprv_ionic_notifications', 
    array( 
        'status' => '1'   // integer (number) 
        ), 
    array( 'notification_id' => $data['notification_id'] ), 
    array( 
        '%d'    // value2
        ), 
    array( '%d' ) 
    );

  if ($result === false) {
    return new WP_Error( 'Notification update Failed', sprintf( __( $wpdb->last_error ) ), array( 'status' => 404 ) );
  }

   return $result;

}

function rew_custom_post_type_rest_support() {
    global $wp_post_types;

    $post_type_name =  bbp_get_reply_post_type();
    if( isset( $wp_post_types[ $post_type_name ] ) ) {
        $wp_post_types[$post_type_name]->show_in_rest = true;
        $wp_post_types[$post_type_name]->rest_base = $post_type_name;
        $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
    }
    $post_type_name =  bbp_get_topic_post_type();
    if( isset( $wp_post_types[ $post_type_name ] ) ) {
        $wp_post_types[$post_type_name]->show_in_rest = true;
        $wp_post_types[$post_type_name]->rest_base = $post_type_name;
        $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
    }
    $post_type_name =  bbp_get_forum_post_type();
    if( isset( $wp_post_types[ $post_type_name ] ) ) {
        $wp_post_types[$post_type_name]->show_in_rest = true;
        $wp_post_types[$post_type_name]->rest_base = $post_type_name;
        $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
    }
    $post_type_name =  'event';
    if( isset( $wp_post_types[ $post_type_name ] ) ) {
        $wp_post_types[$post_type_name]->show_in_rest = true;
        $wp_post_types[$post_type_name]->rest_base = $post_type_name;
        $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
    }     
}

function add_user_data_to_response_after_login($data, $user) {
    $data['user_id'] = $user->ID;
    $data['user_roles'] = $user->roles;
    return $data;
}
function bbpress_rest_api_replies_extra_data( $response, $handler, $request ) {

    $thisIsAppleDevice = isAppleDevice();
	// Add hide-ios class to  join button in about page
	if ( 'page' == $response->data['type'] && 'about' == $response->data['slug'] ) {
        if($thisIsAppleDevice) {

            $content = $response->data['content']['rendered'] ;
            $content = str_replace('<a class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-blue" href="https://novonee.com/join/" title="">Join Today!</a>', '', $content);
            $response->data['content']['rendered'] = $content;
            return $response;   
        }
	}

    if ( 'event' == $response->data['type'] ) {
    // wp_mail('sambhal@outlook.com','res1',gettype( $response->data ).' :: '. json_encode($response). '::'.( $response->data['id'] ));

        global $wpdb;
        $event_data = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'em_events where post_id='.$response->data['id']);
        $event_data[0]->post_content =  do_shortcode($event_data[0]->post_content);   
        
        $content = $event_data[0]->post_content ;
        $content = apply_filters( 'the_content', $content );
        
        if($thisIsAppleDevice && 0) {
            $content = str_replace('NON-MEMBERS CLICK JOIN AT THE TOP OF THIS PAGE', '', $content);

            // Remove event register button
            $content = preg_replace('/<a.*Members CLICK HERE to register.*?\/a>/i','', $content);
            // Remove Youtube link 
            $content = preg_replace('/<a.*Members Click Here to Watch Recording.*?\/a>/i','', $content);
        }

        // If the user is not logged in then remove the join event link and video too from the event
        if(!is_user_logged_in()){

            $content = str_replace( ']]>', ']]&gt;', $content );
            $content = preg_replace('/<iframe.*?\/iframe>/i','<a href="#/app/login" class="button button-register no-user">View Event Video</a>', $content);
            $content = str_replace('class="button button-register"', 'class="button button-register no-user"', $content);
            $content = preg_replace('/<a(.*)href="([^"]*)"(.*)>/','<a$1href="#/app/login"$3>',$content);
            $event_data[0]->post_content = $content;
        } 

        // Get featured image
        $featured_image = get_the_post_thumbnail($response->data['id'], 'medium');
        // Concat the featured image in the content
        $response->data['content']['rendered'] = $featured_image . ' '. $content;
        $response->data['event_data'] = $event_data[0];
        return $response;
    } else {

        foreach ( $response->data as $key => $single_item ) {
        // Only modify replies
            if ( 'event' == $single_item['type'] ) {

                global $wpdb;
                $event_data = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'em_events where  post_id='.$single_item['id']); 
                $response->data[$key]['event_data'] = $event_data;
            }
            if ( 'post' == $single_item['type'] ) {

				// Add hide-ios class to  join button in posts
                if($thisIsAppleDevice) {
                    $content = $response->data[$key]['content']['rendered'] ;
                    $content = str_replace('<a class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-color-blue" href="https://novonee.com/join/" title="">Sign Up Today!</a>', '', $content);
                    $response->data[$key]['content']['rendered'] = $content;
                }
            }

        }
        return $response;   
    }

    return $response;   

}

function sprv_get_events($request,$response){
	require_once(WP_PLUGIN_DIR.'/events-manager/classes/em-events.php');

    $events = EM_Events::get(array('scope'=>'all'));
    $return = [];
    foreach ($events as $key => $value) {
       $return[] = (array)$value;
   }
   header("X-WP-Total: " . sizeof($return));
   return new WP_REST_Response($return, 200);
}

function sprv_get_past_events(){

    $events = EM_Events::get(array('scope'=>'past'));
    $return = [];
    foreach ($events as $key => $value) {
       $return[] = (array)$value;
   }
   header("X-WP-Total: " . sizeof($return));
   return new WP_REST_Response($return, 200);
}

function sprv_get_future_events(){

    $events = EM_Events::get(array('scope'=>'future', 'order'=>'asc'));
    $return = [];
    foreach ($events as $key => $value) {
       $return[] = (array)$value;
   }
   header("X-WP-Total: " . sizeof($return));
   return new WP_REST_Response($return, 200);      
}

function sprv_get_events_from_category($request){
	$cat_id =$request[1];
	$events = EM_Events::get(array('scope'=>'past','category'=>$cat_id));
	$return = [];
	foreach ($events as $key => $value) {
		$return[] = (array)$value;
	}

	$per_page = (isset($request['per_page']) && is_numeric($request['per_page']))?$request['per_page']:10;
	$total = sizeof($return);
	$total_pages = ceil($total/$per_page);
	header("X-WP-Total: " . $total);
	header("X-WP-TotalPages: " . $total_pages);
	return new WP_REST_Response($return, 200);      
}

function sprv_get_event_categories() {
   $category = get_terms('event-categories',array(
    'hide_empty' => false,
    ));
   foreach ($category as $key => $value) {
      $return[] = (array)$value;
  }

  $per_page = (isset($request['per_page']) && is_numeric($request['per_page']))?$request['per_page']:10;
  $total = sizeof($return);
  $total_pages = ceil($total/$per_page);
  header("X-WP-Total: " . $total);
  header("X-WP-TotalPages: " . $total_pages);
  return new WP_REST_Response($return, 200);
}

/**
 *
 * Get Replies by topic
 * 
 */

function get_reply_from_topic($request){
    $id =$request[1];


    $topic = get_post( $id,ARRAY_A); 
    $reply = get_posts( array(
        'post_parent' => $id,
        'post_type'=>'reply',
        'numberposts'=>-1
        ) );


    $return = [];
    $return[] = $topic;
    foreach ($reply as $key => $value) {
        $return[] = (array)$value;
    }
    $per_page = (isset($request['per_page']) && is_numeric($request['per_page']))?$request['per_page']:10;
    $total = sizeof($return);
    $total_pages = ceil($total/$per_page);
    header("X-WP-Total: " . $total);
    header("X-WP-TotalPages: " . $total_pages);
    return new WP_REST_Response($return, 200);      
}

function add_fields_to_JSON() {
//Add featured image
    register_rest_field( 'post',
    'featured_image_src', //NAME OF THE NEW FIELD TO BE ADDED - you can call this anything
    array(
        'get_callback'    => 'get_image_src',
        'update_callback' => null,
        'schema'          => null,
        )
    );

    register_rest_field( 'post',
    'author_name', //NAME OF THE NEW FIELD TO BE ADDED - you can call this anything
    array(
        'get_callback'    => 'sprv_get_author_name',
        'update_callback' => null,
        'schema'          => null,
        )
    );


    register_rest_field( 'post',
    'sprv_content', //NAME OF THE NEW FIELD TO BE ADDED - you can call this anything
    array(
        'get_callback'    => 'sprv_get_content',
        'update_callback' => null,
        'schema'          => null,
        )
    );

    register_rest_field( 'post',
    'sprv_date', //NAME OF THE NEW FIELD TO BE ADDED - you can call this anything
    array(
        'get_callback'    => 'sprv_get_date',
        'update_callback' => null,
        'schema'          => null,
        )
    );

    register_rest_field( 'comment',
    'sprv_date', //NAME OF THE NEW FIELD TO BE ADDED - you can call this anything
    array(
        'get_callback'    => 'sprv_comment_date',
        'update_callback' => null,
        'schema'          => null,
        )
    );

}

function get_image_src( $object, $field_name, $request ) {
    $size = 'medium'; // Change this to the size you want | 'medium' / 'large'
    $feat_img_array = wp_get_attachment_image_src($object['featured_media'], $size, true);
    return $feat_img_array[0];
}

function sprv_get_author_name( $object, $field_name, $request ) {
    $author = get_the_author();
    return $author;
}

function sprv_get_content( $object, $field_name, $request ) {
 $content = get_the_content(); 
 $content = do_shortcode("'". $content ."'");
 return $content;

}

function sprv_get_date( $object, $field_name, $request ) {
    $post_date = get_the_date( 'F j, Y' ); 
    return $post_date;
}

function sprv_comment_date( $object, $field_name, $request ) {
    $post_date = get_comment_date( 'F j, Y',$object['id'] ); 
    return $post_date;
}

function compasshb_do_shortcodes( $object, $field_name, $request ) {

   WPBMap::addAllMappedShortcodes(); // This does all the work
   global $post;
   $post = get_post ($object['id']);
   $output['rendered'] = apply_filters( 'the_content', $post->post_content );
   return $output;
}

function isAppleDevice(){
    //Detect special conditions devices
    $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
    $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
    $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
    $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
    $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

    if( $iPod || $iPhone || $iPad ) {
        return true;
    }
    return false;
}

function sprv_get_iap_products($request){

    $return = [];

    global $wpdb;
    $query  = 'SELECT * FROM ' . $wpdb->prefix . 'iap_products WHERE status = 1;' ;
    $results = $wpdb->get_results( $query );
    $return['results'] = $results;

    return new WP_REST_Response($return, 200);

}

  /**
   *
   * This function returns response with key is_subscription_active set to true 
   *  - if the user has active subscription using IAP 
   *  - If the current user is not a Ios IAP subscriber
   *  - if the user is not logged in
   * It return false if the Ios subscriber's  subscription is expired or .
   *  
   * If user_id get param is passed then it will return data for that user id other it will return data for current logged in user.
   */

function sprv_is_user_ios_subscriber($request){

    
    $result = [];

    $return = [];
    $return['Result'] = true;
    
    $user_id = $request->get_params()['user_id'];
    // If the user is not logged in then return as not subscribed
    if(!is_user_logged_in() && $user_id == null){
        
        $return['Result'] = true;
        $return['is_subscription_active'] = true;
        $return['Message'] = 'User is not logged in.';
        return $return;   
        
    }

    $user = wp_get_current_user() ;
    $current_user_id =  get_current_user_id();
    
    if($user_id != null){
        $current_user_id =  $user_id;
        
    }

    // Get the payment_receipt from the database
    global $wpdb;
    $query = "SELECT payment_receipt from " . $wpdb->prefix . "iap_purchase WHERE user_id = $current_user_id";
    $results = $wpdb->get_results( $query  );

    $receiptData = $results[0]->payment_receipt;

    // If the receipt is found then verify it and return the resposne
    if($receiptData == null ){
        // If the user is not subscribed by apple pay in app purchase we assume that he is a subscriber. we are not checking furhter.
        $return['is_subscription_active'] = true;
        // Remove the user role ios_blocked if the user has.
        if ( in_array( 'ios_blocked', (array) $user->roles ) ) {
            $user->remove_role( 'ios_blocked' );
        }
        return $return;   
    }

    //the password you own,if your IAP is not a subscription,let it empty string(like this:''),else use you own password
    $password = '313eb6525c2a4d6ca4b8643d61c31bd8';


    $appleIAP = new AppleInAppPurchaseVerification($receiptData, $password, true);
    $result = $appleIAP->validateReceipt();

    $result = json_decode($result);

    if ($result->status == 0) {

        // Get the latest receipts
        $latest_receipt = $result->latest_receipt_info;

        // Get the latest receipt from receipts
        $last_latest_receipt = end($latest_receipt);

        // Get the expiry date in the latest receipt
        $last_receipt_expiry_date = $last_latest_receipt->expires_date_ms;



        // Get the current timestamp
        $current_timestamp = time() * 1000;

        // Check if the subscription is expired or not
        if ($current_timestamp > $last_receipt_expiry_date) {
            $return['is_subscription_active'] = false;
            // Add ios_blocked user role to this user
            $user->add_role( 'ios_blocked' ); 
            $reason = '';

            try {
                // Get the expiration intent for expired subscription
                $expiration_intent = $result->pending_renewal_info[0]->expiration_intent;

                switch ($expiration_intent) {
                    case '1':
                        $reason = 'Customer canceled their subscription.';
                        break;

                    case '2':
                        $reason = 'Customer canceled their subscription.';
                        break;

                    case '3':
                        $reason = 'Customer did not agree to a recent price increase.';
                        break;

                    case '4':
                        $reason = 'Product was not available for purchase at the time of renewal.';
                        break;

                    case '5':
                        $reason = ' Unknown error';
                        break;

                }

            } catch (Exception $e) {

            }
        } else {
            $return['is_subscription_active'] = true;
        }

    } else {
        $error_codes = [];
        $error_codes['21000'] = 'The App Store could not read the JSON object you provided.';
        $error_codes['21002'] = 'The data in the receipt-data property was malformed or missing.';
        $error_codes['21003'] = 'The receipt could not be authenticated.';
        $error_codes['21004'] = 'The shared secret you provided does not match the shared secret on file for your account.';
        $error_codes['21005'] = 'The receipt server is not currently available.';
        $error_codes['21006'] = 'This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.';
        $error_codes['21007'] = 'This receipt is from the test environment, but it was sent to the production environment for verification. Send it to the test environment instead.';
        $error_codes['21008'] = 'This receipt is from the production environment, but it was sent to the test environment for verification. Send it to the production environment instead.';
        $error_codes['21010'] = 'This receipt could not be authorized. Treat this the same as if a purchase was never made.';
        $error_codes['21100-21199'] = 'Internal data access error.';

        $return['Result'] = false;
        $return['Status'] = $result->status;
        $return['Message'] = $error_codes[$result->status];
        if ($return['Message'] == null) {
            $return['Message'] = $error_codes['21100-21199'];
        }

    }

    return $return;


}


function get_all_comments_of_activity($activity_id){

    // SELECT * FROM wp_bp_activity where type="activity_comment" and item_id=$activity_id
    global $wpdb;
    $query =  "SELECT * FROM ". $wpdb->prefix ."bp_activity where type='activity_comment' and item_id=$activity_id and is_spam=0;";
    $result = $wpdb->get_results($query);
    
    return $result;
    
    


}


function sprv_add_activity($request){

    $args = $request->get_params();
    $args['type'] = 'activity_update';
    $args['component'] = 'activity';

    // Get current user id 
    if(is_user_logged_in() ) {
        $current_user_id = get_current_user_id();
        $args['user_id'] = $current_user_id;
        
        $result = bp_activity_add($args); 
        if($result){
            $activity = bp_activity_get( array(
                'in' => (int) $result,
                ) );

            $return['Result'] = true;

            $activity = $activity['activities'][0];
            // Prepare actity data 
            $data = array(
            'author'                => $activity->user_id,
            'component'             => $activity->component,
            'content'               => $activity->content,
            'date'                  => 'a few seconds ago' ,
            'id'                    => $activity->id,
            'link'                  => $activity->primary_link,
            'parent'                => $activity->type === 'activity_comment' ? $activity->item_id : 0,
            'prime_association'     => $activity->item_id,
            'secondary_association' => $activity->secondary_item_id,
            'status'                => $activity->is_spam ? 'spam' : 'published',
            'title'                 => $activity->action,
            'type'                  => $activity->type,
        );

            $return['activity'] = $data;
            $return['Message'] = 'Activity posted.';
        } else {
            $return['Result'] = false;
            $return['Message'] = 'Unable to post updates.';
        }
        return $return;
    } else {
        return new WP_Error( 'Add activity Failed.', sprintf( __( "Authentication failed. Please login and try again." ) ), array( 'status' => 404 ) );

    }

}

function sprv_add_activity_comment($request){

    $args = $request->get_params();

    // Get current user id 
    if(is_user_logged_in() ) {
        $current_user_id = get_current_user_id();
        $args['user_id'] = $current_user_id;
        
        $result = bp_activity_new_comment($args); 
        if($result){
           
            $return['Result'] = true;
            $return['Message'] = 'comment posted.';
        } else {
            $return['Result'] = false;
            $return['Message'] = 'Unable to add comment.';
        }
        return $return;
    } else {
        return new WP_Error( 'Add activity comment Failed.', sprintf( __( "Authentication failed. Please login and try again." ) ), array( 'status' => 404 ) );

    }

}


function sprv_get_activity_comments($request){

    $args = $request->get_params();

    // Get current user id 
    if(is_user_logged_in() ) {
        
        global $wpdb;
        $bp = buddypress();

        $comments = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$bp->activity->table_name} WHERE type = 'activity_comment' AND secondary_item_id = %d AND is_spam = 0 ", $args['parent_id'] ) );
        $activity_comment_depth  = get_activity_comments_depth($args['parent_id']);
            
        $bprac = new BP_REST_Activity_Controller();
        foreach ($comments as $key => $comment) {
            $depth = get_activity_comments_depth($comment->id);
            
            $user = get_userdata($comment->user_id);
            $comment->username = $user->display_name;
            $comment->user_avatar = get_avatar( $comment->user_id);;
            // $comment->user_avatar = get_avatar();
            $comment->date_recorded = bp_core_time_since( $comment->date_recorded ) ;
            $comment->depth = $depth;
        }

        if(sizeof($comments)) {
            $return['Result'] = true;
            $return['Depth'] = $activity_comment_depth;
            $return['Comments'] = $comments;
        } else {
            $return['Result'] = false;
            $return['Message'] = 'No comments available.';
        }
        return $return;
    } else {
        return new WP_Error( 'Add activity comment Failed.', sprintf( __( "Authentication failed. Please login and try again." ) ), array( 'status' => 404 ) );

    }

}

function get_activity_comments_depth($parent_id){
    global $wpdb;
    $bp = buddypress();
    $comments_count = $wpdb->get_var( "SELECT COUNT(*) FROM {$bp->activity->table_name} where type='activity_comment' and secondary_item_id=$parent_id and is_spam=0;" );
    return $comments_count;
}

