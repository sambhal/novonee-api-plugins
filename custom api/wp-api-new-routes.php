<?php
/**
 * Plugin Name: WP API New Routes
 * Plugin URI: http://novonee.com
 * Description: This is a custom plugin to add new routes to wp_api
 * Author: Suhail Akhtar
 * Author URI: http://sprvtec.com
 * Version: 0.1
 */
foreach ( glob( dirname(__FILE__) . "/sprv_include/*.php" ) as $endpoint) {
	include_once $endpoint;
}

$namespace = 'sprv/v1';

add_action( 'init',                             'rew_custom_post_type_rest_support', 25 );
add_action( 'rest_api_init',                    'add_fields_to_JSON' ); // Add Fields to API in json response
add_filter( 'rest_request_after_callbacks',     'bbpress_rest_api_replies_extra_data' );
add_filter( 'rest_allow_anonymous_comments',    '__return_true' );
add_filter( 'jwt_auth_token_before_dispatch',   'add_user_data_to_response_after_login', 10, 2 );

/**
 * Modify REST API content for pages to force
 * shortcodes to render since Visual Composer does not
 * do this
 */
add_action( 'rest_api_init', function () {
 register_rest_field(
  'page',
  'content',
  array(
   'get_callback'    => 'compasshb_do_shortcodes',
   'update_callback' => null,
   'schema'          => null,
   )
  );
});

/**
 * Modify REST API content for posts to force
 * shortcodes to render since Visual Composer does not
 * do this
 */
add_action( 'rest_api_init', function () {
 register_rest_field(
  'post',
  'content',
  array(
   'get_callback'    => 'compasshb_do_shortcodes',
   'update_callback' => null,
   'schema'          => null,
   )
  );
});
/*
 * Register all routes
*/
add_action( 'rest_api_init', function () {
    global $namespace;

	$args1 = array(
		'methods' => WP_REST_Server::READABLE,
		'callback' => 'sprv_get_homepage_images',
	);
	register_rest_route( $namespace, '/'.'GetHomepageImages', $args1 );
	

	$args2 = array(
		array(
			'args' => array(
				'user_id' => array(
					'required' => True,
					'description' => 'ID of the user.',
					'type' => 'integer',
				),
				'device_token' => array(
					'required' => True,
					'description' => 'Token of the user device.',
					'type' => 'string',
				),
			),
			'methods' => WP_REST_Server::CREATABLE,
			'callback' => 'sprv_save_device_token',
		),
	);
	
	register_rest_route( $namespace, '/'.'saveDeviceToken', $args2 );

	$args3 = array(
		'methods' => WP_REST_Server::READABLE,
		'callback' => 'sprv_get_notifications',
		'args' =>  array(
				'page' => array(
					'default' 			 => 1,
					'type'               => 'integer',
				),
				'per_page' => array(
					'default' 			 => 20,
					'type'               => 'integer',
				)
			)
		);
	register_rest_route( $namespace, '/'.'notifications', $args3 );
	
	 register_rest_route($namespace, '/' . 'event-categories', array(
        'methods' => 'GET',
        'callback' => 'sprv_get_event_categories',
        ));

    register_rest_route($namespace, '/' . 'events', array(
        'methods' => 'GET',
        'callback' => 'sprv_get_events',
        ));
    register_rest_route($namespace, '/' . 'past-events', array(
        'methods' => 'GET',
        'callback' => 'sprv_get_past_events',
        ));
    register_rest_route($namespace, '/' . 'future-events', array(
        'methods' => 'GET',
        'callback' => 'sprv_get_future_events',
        ));
    register_rest_route( $namespace,  '/' . '/event-categories/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'sprv_get_events_from_category',
        'args' => array(
          'id' => array(       
              ),
          ),
        ) );
    register_rest_route( $namespace,  '/' . '/topic/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'get_reply_from_topic',
        'args' => array(
          'id' => array(       
              ),
          ),
        ) );

    // Route for changing the notification status to read
    register_rest_route( $namespace, '/'.'notification-set-read', array(
      array(
        'args' => array(
          'notification_id' => array(
            'required' => True,
            'description' => 'ID of the notification.',
            'type' => 'integer',
            ),
          ),
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'sprv_notification_set_read',
        ),
      ) );

  // Route for getting the Id of IAP products for ios
  register_rest_route( $namespace, '/'.'GetIapProducts', array(
    'methods' => WP_REST_Server::READABLE,
    'callback' => 'sprv_get_iap_products',
  ) );

  register_rest_route( $namespace, '/'.'CheckIosSubscription', array(
    'methods' => WP_REST_Server::READABLE,
    'callback' => 'sprv_is_user_ios_subscriber',
    'args' => array(
          'user_id' => array(  
              'type' => 'integer'
              ),
          ),
  ) );


  // Rest Route to post a new activity 
    register_rest_route( $namespace, '/'.'AddActivity', array(
      array(
        'args' => array(
          'content' => array(
            'required' => True,
            'description' => 'Content of activity.',
            'type' => 'string',
            ),
          ),
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'sprv_add_activity',
        ),
      ) );

  // Rest Route to post a new activity commnet
    register_rest_route( $namespace, '/'.'AddActivityComment', array(
      array(
        'args' => array(
          'content' => array(
            'required' => True,
            'description' => 'Content of activity.',
            'type' => 'string',
            ),
          'activity_id' => array(
            'required' => True,
            'description' => 'The ID of the root activity item, ie the oldest ancestor of the comment.',
            'type' => 'integer',
            ),
          'parent_id' => array(
            'required' => false,
            'description' => 'The ID of the parent activity item, ie the item to which the comment is an immediate reply. If not provided, this value defaults to the $activity_id.',
            'type' => 'integer',
            ),
          ),
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'sprv_add_activity_comment',
        ),
      ) );

  // Rest Route to post a new activity commnet
    register_rest_route( $namespace, '/'.'GetActivityComments', array(
      array(
        'args' => array(
          'parent_id' => array(
            'required' => true,
            'description' => 'ID of an activity or activity comment.',
            'type' => 'integer',
            ),
          ),
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'sprv_get_activity_comments',
        ),
      ) );

});

