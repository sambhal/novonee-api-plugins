<?php

class BP_API_Friends extends WP_REST_Controller {
    
    /**
    * Register the routes for the objects of the controller.
    */
	public function register_routes() { 
    
		register_rest_route( BP_API_SLUG, '/friends', array(
			array(
				'methods'         => WP_REST_Server::READABLE,
				'callback'        => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'bp_friends_permission' ),
                'args' => array(
                    'context'               => array(
                        'default'           => 'view',
                    ),
                    'sort'                  => array(
                        'default'           => 'alphabetically'
                    ),
                    'filter'                => array(),
                )
			)
		) );
    }
    
	/**
	 * Get all friends
	 *
	 * @param WP_REST_Request $request
	 * @return array|WP_Error
	 */
	public function get_items( $request ) {
        
        $get_function = 'friends_get_' . $request['sort'];
        
        $friends = $get_function(bp_loggedin_user_id());
        
        if(!$friends) {
            return new WP_Error( 'bp_json_friends', __( 'No Friends Found.', BP_API_PLUGIN_SLUG ), array( 'status' => 200 ) );
        }
	
        $data = apply_filters( 'bp_json_prepare_friends', $friends );

		return new WP_REST_Response( $data, 200 );
	
	}
	
	public function get_item( $request ) {
	
		$response = 'a single profile field';
	
		return $response;
	
	}
    
    /**
	 * bp_friends_permission function.
	 *
	 * allow permission to access data
	 * 
	 * @access public
	 * @return void
	 */
	public function bp_friends_permission() {
	
		$response = apply_filters( 'bp_friends_permission', true );
		
		return $response;
    }
	
	
}
