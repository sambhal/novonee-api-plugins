<?php

class BP_API_Messages extends WP_REST_Controller {
    
    /**
    * Register the routes for the objects of the controller.
    */
	public function register_routes() { 
    
		register_rest_route( BP_API_SLUG, '/messages', array(
			array(
				'methods'         => WP_REST_Server::READABLE,
				'callback'        => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'bp_messages_permission' ),
                'args' => array(
                    'context'               => array(
                        'default'           => 'view',
                    ),
                    'box'                  => array(
                        'default'           => 'inbox'
                    ),
                    'filter'                => array(),
                )
			),
            array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'create_item' ),
                'permission_callback' => array( $this, 'bp_messages_permission' ),
                'args'            => $this->get_endpoint_args_for_item_schema( true ),
            )
		) );
        register_rest_route( BP_API_SLUG, '/messages/(?P<id>\d+)', array(         
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'get_item' ),
                'permission_callback' => array( $this, 'bp_messages_permission' ),
                'args' => array(
                    'id' => array(
                         // @todo
//                        'validate_callback' => 'is_numeric'
                    ),
                )
		    ),
			array(
				'methods'         => WP_REST_Server::EDITABLE,
				'callback'        => array( $this, 'update_item' ),
				'permission_callback' => array( $this, 'bp_messages_permission' ),
				'args'            => array_merge( $this->get_endpoint_args_for_item_schema( false ), array(
					'action'    => array() /* read, unread, star, unstar */
				) ),
			),
            array(
                'methods'  => WP_REST_Server::DELETABLE,
                'callback' => array( $this, 'delete_item' ),
                'permission_callback' => array( $this, 'bp_messages_permission' ),
                'args' => array(
                    'id' => array(
                        //@todo
//                        'validate_callback' => 'is_numeric'
                    ),
                )
            )
        ) );
        register_rest_route( BP_API_SLUG, '/public_message', array(
            array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'create_public_item' ),
                'permission_callback' => array( $this, 'bp_messages_permission' ),
                'args'            => $this->get_endpoint_args_for_item_schema( true ),
            )
		) );
		register_rest_route( BP_API_SLUG, '/private_messages', array(
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'get_private_messages' ),
                'args' => array(
                	'per_page'=> 100,
                	'max' => false,
                	'type' => 'all'

                	)
            )
		) );		
		register_rest_route( BP_API_SLUG, '/private_messages/view/(?P<id>\d+)', array(
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'get_private_messages_view' ),
            )
		) );
	}

	/**
	 * Update a single message
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function update_item( $request ) {
		$id = (int) $request['id'];
        
        if ( !messages_check_thread_access( $id ) ) {
            return new WP_Error( 'bp_json_message_invalid_id', __( 'Message ID is invalid.' ), array( 'status' => 400 ) );
        }

        switch ($request['action']) {
            case 'read':
                messages_mark_thread_read( $id );
                break;
            case 'unread':
                messages_mark_thread_unread( $id );
                break;
            case 'star':
                $thread = new BP_Messages_thread( $id );
                $mids = wp_list_pluck( $thread->messages, 'id' );
				bp_messages_star_set_action( array(
					'action'     => 'star',
					'message_id' => $mids[0],
				) );
                break;
            case 'unstar':
                bp_messages_star_set_action( array(
					'action'    => 'unstar',
					'thread_id' => $id,
					'bulk'      => true,
					'order' 	=> 'DESC' 
				) );
                break;
            /* Single message */
//            case 'star_single':
//                bp_messages_star_set_action( array(
//                    'action'     => 'star',
//                    'message_id' => $id,
//                ) );
//                break;
//            case 'unstar_single':
//                bp_messages_star_set_action( array(
//                    'action'     => 'unstar',
//                    'message_id' => $id,
//                ) );
//                break;
        }
        
		$response = $this->get_item( array(
			'id'      => $id,
			'context' => 'edit',
		));

		return rest_ensure_response( $response );

	}    
    
	/**
	 * Create a single message
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function create_item( $request ) {
		if ( ! empty( $request['id'] ) ) {
			return new WP_Error( 'bp_json_message_exists', __( 'Cannot create existing message.', BP_API_PLUGIN_SLUG ), array( 'status' => 400 ) );
		}

		$message = $this->prepare_item_for_database( $request );
		
        $message_id = messages_new_message( array(
            'sender_id'  => ($message->sender_id)?$message->sender_id:bp_loggedin_user_id(),
            'thread_id'  => $message->thread_id,   // false for a new message, thread id for a reply to a thread.
            'recipients' => $message->recipients, // Can be an array of usernames, user_ids or mixed.
            'subject'    => $message->subject, // empty for a reply to a thread
            'content'    => $message->content,
            'date_sent'  => ($message->date_sent)?$message->date_sent:bp_core_current_time(),   
        ));

        if ( ! $message_id ) {
            return new WP_Error( 'bp_json_message_create', __( 'Error creating new message.', BP_API_PLUGIN_SLUG ), array( 'status' => 500 ) );
        }
		

		$this->update_additional_fields_for_object( $message, $request );

		/**
		 * Fires after a message is created via the REST API
		 *
		 * @param object $message Data used to create message
		 * @param WP_REST_Request $request Request object.
		 * @param bool $bool A boolean that is false.
		 */
		do_action( 'bp_json_insert_message', $message, $request, false );
		do_action_ref_array( 'messages_message_sent', array( &$message ) ); 
		
		$response = $this->get_item( array(
			'id'      => $message_id,
			'context' => 'view',
		));
		$response = rest_ensure_response( $response );
		$response->set_status( 201 );
		$response->header( 'Location', rest_url( '/users/' . $message_id ) );

		return $response;
	}
       
	/**
	 * Create a single public message
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function create_public_item( $request ) {
		$params = $request->get_params();

		$user_link = bp_core_get_userlink( $params['item_id'] );

		$args = [];
		$args['content'] = bp_activity_at_name_filter( $params['content'] ) ;
        $args['user_id'] = $params['user_id']; 
		$args['component'] = 'activity'; //activity
		$args['type'] = 'activity_update'; //activity_update
		$message = $this->prepare_item_for_database( $request );
		
         $activity_id = bp_activity_add( $args );

        if ( ! $activity_id ) {
            return new WP_Error( 'bp_json_activity_create', __( 'Error creating new activity.', BP_API_PLUGIN_SLUG ), array( 'status' => 500 ) );
        }
		
        $args['id'] = $activity_id;
        
        do_action( 'bp_activity_add', $args ); 
		$this->update_additional_fields_for_object( $message, $request );

		/**
		 * Fires after a message is created via the REST API
		 *
		 * @param object $message Data used to create message
		 * @param WP_REST_Request $request Request object.
		 * @param bool $bool A boolean that is false.
		 */

		$response = rest_ensure_response( $activity_id );
		$response->set_status( 201 );
		return $response;
		// return  new WP_REST_Response($activity_id,200);

	}
        
	/**
	 * Get all messages
	 *
	 * @param WP_REST_Request $request
	 * @return array|WP_Error
	 */
	public function get_items( $request ) {
        /* filter messages */
        $args                   = array();
		$args['box']          = $request['box'];
        
        if($request['box'] == 'starred') {
            $args['meta_query'] = array( array(
                'key'   => 'starred_by_user',
                'value' => bp_loggedin_user_id()
            ) );           
        }

		if ( isset( $request['filter'] ) ) {
			$args = array_merge( $args, $request['filter'] );
			unset( $args['filter'] );
		}
        
		global $messages_template;
		$data = array();
		if ( bp_has_message_threads($args) ) {
			while ( bp_message_threads() ) : bp_message_thread();
				$single_msg = array(
				    'thread_id'         => $messages_template->thread->thread_id,
				    'read'              => ( bp_message_thread_has_unread() )? false : true,
				    'total_messages'    => bp_get_message_thread_total_count( $messages_template->thread->thread_id ),
				    'unread_messages'   => bp_get_message_thread_unread_count( $messages_template->thread->thread_id ),
				    'avatar'            => bp_core_fetch_avatar( array( 'item_id' => $messages_template->thread->last_sender_id,'width' => 25, 'height' => 25, 'html' => false ) ),
				    'from'              => bp_core_get_username($messages_template->thread->last_sender_id),
				    'from_id'           => $messages_template->thread->last_sender_id,
				    'last_message_date' => $messages_template->thread->last_message_date,
				    'subject'           => bp_get_message_thread_subject(),
				    'excerpt'           => bp_get_message_thread_excerpt(),
				    'content'           => bp_get_message_thread_content()
                );
                if(bp_is_active( 'messages', 'star' )) 
                $single_msg['star'] = strpos( bp_get_the_message_star_action_link( array( 'thread_id' => bp_get_message_thread_id() ) ), 'unstar' )?true:false;
            
                if($args['box'] == 'sentbox') {
                    foreach($messages_template->thread->recipients as $user=> $userdata){
                        if ( (int) $user !== bp_loggedin_user_id() ) {
                            $single_msg['to'][$user]['name'] = bp_core_get_username( $user );
                        }
                    }
                }
                $links['self'] = array(
                            'href' => rest_url( sprintf( BP_API_SLUG.'/messages/%d', $messages_template->thread->thread_id ) ),
                        );
                if($args['box'] == 'sentbox') {
                    $links['collection'] = array(
                                'href' => rest_url( BP_API_SLUG.'/messages?box=sentbox' ),
                            );
                } else {
                    $links['collection'] = array(
                            'href' => rest_url( BP_API_SLUG.'/messages/' ),
                        );
                }
				$single_msg['_links']=$links;
				$data[]=$single_msg;
			endwhile;
        } else {
            return new WP_Error( 'bp_json_messages', __( 'No Messages Found.', BP_API_PLUGIN_SLUG ), array( 'status' => 200 ) );
        }
        
        $data = apply_filters( 'bp_json_prepare_messages', $data );

		return new WP_REST_Response( $data, 200 );
	}
				
	/**
	 * Get a specific message
	 *
	 * @param WP_REST_Request $request
	 * @return array|WP_Error
	 */
	public function get_item( $request ) {
        global $thread_template;
        $id = $request['id'];
		if(bp_thread_has_messages(array('thread_id'=>$id))) {
            $data = array();
			$data['thread_id'] = $id;
			$data['subject'] = bp_get_the_thread_subject();
            if(bp_is_active( 'messages', 'star' )) 
            $data['star'] = strpos( bp_get_the_message_star_action_link( array( 'thread_id' =>$id ) ), 'unstar' )?true:false;
			if ( bp_get_thread_recipients_count() <= 1 ) {
                $data['thread_title'] = __('You are alone in this conversation.', BP_API_PLUGIN_SLUG );
            } elseif ( bp_get_max_thread_recipients_to_list() <= bp_get_thread_recipients_count() ) {
                $data['thread_title'] = sprintf( __('Conversation between %s recipients.', BP_API_PLUGIN_SLUG ), number_format_i18n( bp_get_thread_recipients_count()) );
            } else {
                foreach( (array) $thread_template->thread->recipients as $recipient ) {
                    if ( (int) $recipient->user_id !== bp_loggedin_user_id() ) {
                        $recipient_name = bp_core_get_user_displayname( $recipient->user_id );

                        if ( empty( $recipient_name ) ) {
                            $recipient_name = __( 'Deleted User', BP_API_PLUGIN_SLUG );
                        }

                        $recipient_names[] = $recipient_name;
                    }
                }
                $data['thread_title'] = sprintf( __('Conversation between %s and you.' ), implode(',', $recipient_names) );
                $data['thread_msg'] = [];
                
                while ( bp_thread_messages() ) : bp_thread_the_message();

                    $single_msg = (array)$thread_template->message;
                    $single_msg['sender_avatar'] = bp_core_fetch_avatar( array( 'item_id' => $thread_template->message->sender_id,'width' => 25, 'height' => 25, 'html' => false ) );
                    $single_msg['sender_name'] = bp_get_the_thread_message_sender_name();
                    if(bp_is_active( 'messages', 'star' )) 
                    $single_msg['star'] = bp_messages_is_message_starred($thread_template->message->id, bp_loggedin_user_id()); 
                
                    $data['thread_msg'][] = $single_msg;

                endwhile;
            }
            
        } else {
            return new WP_Error( 'bp_json_message', __( 'Message Not Found.', BP_API_PLUGIN_SLUG ), array( 'status' => 404 ) );
        }
        
		return new WP_REST_Response( $data, 200 );	
    }
    
    /**
	 * Delete a single message
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function delete_item( $request ) {
        $id = (int) $request['id'];
        
        if ( ! bp_thread_has_messages(array('thread_id'=> $id)) ) {
			return new WP_Error( 'bp_json_message', __( 'Message Not Found.', BP_API_PLUGIN_SLUG ), array( 'status' => 404 ) );
		}
        
        $deleted = messages_delete_thread($id);
        
		if ( ! $deleted ) {
			return new WP_Error( 'bp_json_message_cannot_be_deleted', __( 'The message cannot be deleted.', BP_API_PLUGIN_SLUG ), array( 'status' => 500 ) );
		}
        
		return new WP_Error( 'bp_json_message_deleted', __( 'Message deleted successfully.', BP_API_PLUGIN_SLUG ), array( 'status' => 200 ) );
	}
        
    /**
	 * Get all private messages
	 *	It returns the subject of the message not the actual thread
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_Error|WP_REST_Response
	 */
    public function get_private_messages( $request ) {

    	if(!is_user_logged_in()){
    		return new WP_Error( 'Authentication Failed', sprintf( __( 'Please login before continue.' ) ), array( 'status' => 404 ) );
    	}

    	$messages=  [];
    	$j=0;
    	$args = [];
    	$args['per_page'] = $request['per_page'];
        $args['max'] = $request['max'];
        $args['type'] = $request['type'];
    	
    	if ( bp_has_message_threads( $args ) ) {
    		while (bp_message_threads()) {
    			bp_message_thread();
    			$msg[$j]  ;
    			$msg[$j]['thread_id']  		=  bp_get_message_thread_id();
    			$msg[$j]['avatar'] 		=  bp_get_message_thread_avatar();;
    			$msg[$j]['last_post_date'] 		=  bp_core_time_since(bp_get_message_thread_last_post_date_raw());;
    			$msg[$j]['subject'] 		=  bp_get_message_thread_subject();;
    			$msg[$j]['content'] 		 =  bp_get_message_thread_content();
    			$msg[$j]['total_count'] 		 =  bp_get_message_thread_total_count();
    			$msg[$j]['last_post_date'] 		 =  bp_get_message_thread_last_post_date();
    			$msg[$j]['unread_count']  		=  bp_get_message_thread_unread_count();
    			$msg[$j]['from_id']  		=  $this->bp_get_message_thread_from_id()['id'];
    			$msg[$j]['from_name'] 		 =  $this->bp_get_message_thread_from_id()['name'];
    			$j++;
    		}
    		return $msg;
    	}
    }

	/**
	 * Get all private messages
	 *	It returns the actual thread of a private message
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_Error|WP_REST_Response
	 */
    public function get_private_messages_view($request){

    	if(!is_user_logged_in()){
			return new WP_Error( 'Authentication Failed', sprintf( __( 'Please login before continue.' ) ), array( 'status' => 404 ) );
		}

    	$params = $request->get_params();
    	$thread_id = $params['id'];

    	if(!$thread_id){
			return new WP_Error( 'Thread id not found.', sprintf( __( 'Requested thread not found.' ) ), array( 'status' => 404 ) );
    	}

    	$msg = [];
    	$j=0;

    	$args = array(
    		'thread_id'     => $thread_id
    		);
    	if ( bp_thread_has_messages( $args ) ) {
    		while (bp_thread_messages()) {
    			bp_thread_the_message();

    			global $thread_template;
    			$recipients = $thread_template->thread->recipients;
    			$rec = [];
    			foreach ($recipients as $key => $recipient) {
    				$rec[] = $key;
    			}

    			$msg[$j]  = [];
    			$msg[$j]['thread_id']  		= bp_get_the_thread_id();
    			$msg[$j]['subject'] 		 = bp_get_the_thread_subject();
    			$msg[$j]['message']  		= bp_get_the_thread_message_content();
    			$msg[$j]['msg_id'] 		 = bp_get_the_thread_message_id();
    			$msg[$j]['avatar_thumb'] 		 = bp_get_the_thread_message_sender_avatar_thumb();
    			$msg[$j]['sender_id'] 		 = bp_get_the_thread_message_sender_id();
    			$msg[$j]['sender_name']  		= bp_get_the_thread_message_sender_name();
    			$msg[$j]['time_since']  		= bp_get_the_thread_message_time_since();
    			// $msg[$j]['recipients'] 		 = bp_get_the_thread_recipients();
    			$msg[$j]['recipients'] 		 = $rec;
    			$j++;
    		}
    		return $msg;
    	}
    }
    /**
	 * bp_messages_permission function.
	 *
	 * allow permission to access data
	 * 
	 * @access public
	 * @return void
	 */
	public function bp_messages_permission() {
	
		$response = apply_filters( 'bp_messages_permission', true );
		
		return $response;
	
	}
    
	/**
	 * Prepare a single message for create or update
	 *
	 * @param WP_REST_Request $request Request object.
	 * @return object $message User object.
	 */
	protected function prepare_item_for_database( $request ) {
		$message = new stdClass;  

		// required arguments.
		if ( isset( $request['recipients'] ) ) {
			$message->recipients = $request['recipients'];
		}

		// optional arguments.
        if ( isset( $request['sender_id'] ) ) {
			$message->sender_id = absint($request['sender_id']);
		}
        if ( isset( $request['thread_id'] ) ) {
			$message->thread_id = absint($request['thread_id']);
		}
		if ( isset( $request['subject'] ) ) {
			$message->subject = $request['subject'];
		}
		if ( isset( $request['content'] ) ) {
			$message->content = $request['content'];
		}
		if ( isset( $request['date_sent'] ) ) {
			$message->date_sent = $request['date_sent'];
		}

		/**
		 * Filter user data before inserting user via REST API
		 *
		 * @param object $message Message object.
		 * @param WP_REST_Request $request Request object.
		 */
		return apply_filters( 'bp_json_pre_insert_message', $message, $request );
	}

	public function bp_get_message_thread_from_id() { 
    	global $messages_template; 
 		
    	$user = [];
    	$user['id']   = $messages_template->thread->last_sender_id;
    	$user['name'] = get_userdata($user['id'])->display_name;

    	return $user; 
	} 

}